FROM python:3.9.1-alpine3.13
LABEL version="1.0"
LABEL description="eve_spsolutions eve"
LABEL maintainer = ["eduardo_jonathan@outlook.com"]
WORKDIR /code
#RUN apk add --no-cache gcc musl-dev linux-headers
RUN apk add --no-cache build-base
EXPOSE 8090/tcp
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY ./index.py .
COPY ./settings.py .
#CMD ["flask", "run"]
# Specify the command to run on container start
CMD ["python", "./index.py"]