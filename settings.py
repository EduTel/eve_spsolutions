
# -*- coding: utf-8 -*-
MONGO_URI = 'mongodb+srv://UserEve_spsolutions:JT0PmNqIwPnX8bRa@cluster0.c4djo.mongodb.net/eve_spsolutions?retryWrites=true&w=majority'
#MONGO_URI = 'mongodb://user:user@localhost:27017/evedemo'


# Enable reads (GET), inserts (POST) and DELETE for resources/collections
# (if you omit this line, the API will default to ['GET'] and provide
# read-only access to the endpoint).
RESOURCE_METHODS = ['GET', 'POST', 'DELETE']

# Enable reads (GET), edits (PATCH) and deletes of individual items
# (defaults to read-only item access).
ITEM_METHODS = ['GET', 'PATCH', 'DELETE']


user = {
    # 'title' tag used in item links.
    'item_title': 'users',

    # Schema definition, based on Cerberus grammar. Check the Cerberus project
    # (https://github.com/pyeve/cerberus) for details.
    'schema': {
        'firstname': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 10,
            'required': True,
        },
        'lastname': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 15,
            'required': True,
        },
        # 'role' is a list, and can only contain values from 'allowed'.
        'role': {
            'type': 'list',
            'allowed': ["admin", "user"],
        }
    }
}

homes = {

    'schema': {
        'direction': {
            'type': 'string',
            'required': True,
        },
        'description': {
            'type': 'string',
            'required': True,
        },
        'owner': {
            'type': 'objectid',
            'required': True,
            # referential integrity constraint: value must exist in the
            # 'user' collection.
            'data_relation': {
                'resource': 'user',
                # make the owner embeddable with ?embedded={"owner":1}
                'embeddable': True
            },
        },
    }
}

# The DOMAIN dict explains which resources will be available and how they will
# be accessible to the API consumer.
DOMAIN = {
    'user': user,
    'homes': homes,
}