# Pasos para su instalacion en local
pip install -r requirements.txt  
python index.py  

# Pasos para su construccion
debido a la falta de especificasiones me vi limitado en poder trabajar ya que  
no sabia que producto les podria gustar o que aportacion seria tomada con agrado  
o si solo habia un solo camino que tomar. entonces de las diferentes formas que se  
podia hacer, decidi usar python ya que la vacante es de python, tambien decidi  
usar flask ya que es uno de framework preferido en python para microservicios,  
decidido a hacer una app mas haya de solo usar links para la parte de **HATEOAS**  
como lo podria hacer **DRF**, decidi usar un framework mas especialisado que funciona
sobre flask, **EVE** https://docs.python-eve.org/_/downloads/en/stable/pdf/ te da un estandar y un gran punto de partida  
para las caracteristicas de hypermedia.  

# Alternativas
RIPOZO
Serverless Framework
DRF

# Base de datos
use mongo atlas de igual manera pude usar mongo db de manera local con docker-compose
o de ambas formas configurandole por variable de entorno

# Pasos 
`git init`  
`python -m venv .venv`  
`source .venv/bin/activate`  
`pip install eve`
`pip install dnspython`  
`pip install flask-cors`  
`pip install zappa `  
`zappa init`  
`zappa deploy dev`  
 
# Estandar
pep8

# Puntos de la practica
4. Despliega el API REST en tu máquina.  
`python index.py`  

5. Prueba el API con el cliente REST que más te guste. Documenta.  
GET Json  
![out json](/img/out%20json.png)
GET XML  
![out xml](/img/out%20xml.png)
POST  
![post](/img/post.png)
    * use postman aunque de igual manera podria usar curl o postwoman

6. Conteneriza la aplicación y levanta el contenedor. Expón tu servicio por el puerto 8090. 
    * `docker build -t eve_spsolutions:0.0.1 .`
    ![images docker](/img/images%20docker.png)
    * `docker run -it --publish 8090:8090 eve_spsolutions:0.0.1`
    ![container docker](/img/container%20docker.png)

7. Prueba el API, ahora corriendo en el contenedor. Documenta de nuevo. 
Documenta.
![container get](/img/container%20get.png)

8. Ahora despliega tu aplicación en un entorno local de kubernetes (minikube) con al menos 2 pods.
    * Cambiar el tag para subirlo a docker Hub
        * `docker tag eve_spsolutions:0.0.1 edutel/eve_spsolutions:0.0.1`  
    * subir a docker Hub
        * `docker push edutel/eve_spsolutions:0.0.1`  
        ![docker hub](/img/docker%20hub.png)
    * `kubectl create -f ./deployment.yaml` o `kubectl create deployment spsolutions --image=edutel/spsolutions:0.0.1 --replicas=2`  
    deployment  
    ![pods](/img/deployment.png)
    pods  
    ![pods](/img/pods.png)

9. Prueba el API, ahora corriendo en minikube. Documenta de nuevo. 
* `kubectl apply -f svc.yaml` o `kubectl expose deployment evespsolutions-deployment --type=NodePort --port=8090`  
servicio  
![pods](/img/servicio.png)  
mostrar servicio  `minikube service evespsolutions-deployment`  
![pods](/img/minikube%20service.png)  
url pod  
![pods](/img/minikube%20url.png)  

10. Detén alguno de los pods. Documenta de nuevo  
`kubectl delete pod rc-evespsolutions-9xg5k`  
![delete pod](/img/delete%20pod.png)  
Se creo un nuevo pod  
![nuevo pod](/img/nuevo%20pod.png)  


11. Ahora despliega tu aplicación en un servicio FaaS o SaaS en la nube. (Prueba con un free tier de AWS, Google, Azure,
Oracle). ¿No tienes tarjeta de crédito? Intenta con estos servicios: https://platform.cloudways.com/signup,
https://replit.com/, https://fly.io/, 
* FaaS  
    https://github.com/zappa/Zappa  
    `pip install zappa`  
    `zappa init`  
    `zappa deploy`  
url: https://9kpzalv3k5.execute-api.us-east-1.amazonaws.com/dev/

12. Prueba el API, ahora corriendo en servicio. Documenta de nuevo. 
![lambda](/img/lambda.png)
![aws](/img/aws.png)

# Extras
* Podrías agregar un endpoint de salud, avísale al mundo que tu endpoint esta saludable... es la mejor
manera de ayudar a alguna herramienta de automatización. Puedes utilizar una librería acorde al lenguaje
que estés utilizando.  
Path: /healthcheck  
![hc](/img/hc.png)  
monitorear status: https://app.statuscake.com/  
![statuscake](/img/statuscake.png)  
* ¿Ese contenedor quedó muy gordo?, seguro puedes ayudarlo a ser más ligero.

    se ocupa una distrubucion pequeña de linux alpine3 y solo se importan los archivos necesarios  
    apk: “–no-cache” que evita guardar archivos descargados como indices de paquetes o temporales  

* Los servicios REST tienen varias formas para documentarse, elige una estándar y ayuda a que haya más
servicios REST documentados de manera adecuada.  

    se encuentra documentado con **swagger** haciendo uso de swagger-ui  
    para su mayor entendimiento  
    `docker-compose build`  
    `docker-compose up -d`  
    ![swagger-ui](/img/swagger-ui.png)  