from eve import Eve
from eve_swagger import get_swagger_blueprint, add_documentation
from eve_healthcheck import EveHealthCheck

app = Eve()
swagger = get_swagger_blueprint()
app.register_blueprint(swagger)
hc = EveHealthCheck(app, '/healthcheck')

# required. See http://swagger.io/specification/#infoObject for details.
app.config['SWAGGER_INFO'] = {
    'title': 'eve_spsolutions',
    'version': '0.0.1',
    'description': 'eve_spsolutions',
    'contact': {
        'name': 'edutel',
        'url': 'https://www.linkedin.com/in/eduardo-tellez-0aaab9108/'
    },
    'schemes': ['http', 'https'],
}

if __name__ == '__main__':
    app.run(port=8090, host='0.0.0.0', debug=True)